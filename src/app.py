#!/usr/bin/env python3

"""
Simple weather application
See README.md
"""
from concurrent.futures import ThreadPoolExecutor as PoolExecutor
from multiprocessing import cpu_count
from pprint import pformat
import itertools
import logging
from cachetools.func import lru_cache, ttl_cache
from requests import get, exceptions
from retry import retry
from flask import Flask
import settings

app = Flask(__name__)
log = logging.getLogger() or app.logger

app.config.from_object(settings.Settings)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)


@app.before_first_request
def print_config():
    """log currently used config object"""
    log.info(pformat(app.config))


@lru_cache(maxsize=1)
@app.route('/health', methods=['GET'])
def health():
    """print empty json object and give HTTP status code 200"""
    log.debug('health-check successful')
    return '{}'


@ttl_cache(maxsize=2, ttl=30)
@app.route('/temperature', methods=['GET'])
def get_avg_data():
    """main endpoint"""
    return {
        "avg_temperature": avg(get_data())
    }


def avg(numbers):
    """
    :param numbers: list of numeric values
    :return: average value over the list
    """
    return sum(numbers) / float(len(numbers))


def get_data():
    """
    :return: filtered list of temperatures for configured boxes
    """
    return fetch_async(app.config['BOXES'])


def flatten(list_of_lists):
    """
    Flatten one level of nesting
    :param list_of_lists:
    :return: list
    """
    return list(itertools.chain.from_iterable(list_of_lists))


def fetch_async(boxes=None):
    """
    Asynchronously fetches data from specified list of boxes
    :param boxes: list of box_ids from openSenseMap
    :return: list of temperatures for all boxes
    """
    if boxes is None:
        boxes = []
    with PoolExecutor(max_workers=min(cpu_count() * 2, len(boxes))) as pool:
        all_temperatures = list(pool.map(get_single_box_data, boxes))
    return flatten(all_temperatures)


def fahrenheit_to_celsius(temp_f):
    """
    convert ⁰F => ⁰C
    No typed values here yet
    """
    return (temp_f - 32.0) * 5.0 / 9.0


def get_celsius(box_sensors=None):
    """
    Filters sensors by icon field (as more reliable than guessing by Name field)
    :param box_sensors: list of sensors for the box
    :return: list of temperatures in Celsius
    """
    if box_sensors is None:
        box_sensors = {}
    temperatures = []
    for sensor in box_sensors:
        if sensor.get('icon', '') == 'osem-thermometer':
            temperatures.append(align_units(sensor))
    return list(filter(is_numeric, temperatures))


def is_numeric(any_type):
    """
    :param any_type:
    :return: bool: is parameter numeric?
    """
    return isinstance(any_type, (int, float))


def align_units(sensor):
    """
    Guessing sensor units (dropping unknown) and converting Fahrenheit to Celsius
    :param sensor: object containing sensor data
    :return: Temperature measured by this sensor last time (in Celsius)
    """
    unit = sensor.get('unit', '')
    if unit in app.config['CELSIUS_LABELS']:
        last = sensor.get('lastMeasurement', '')
        log.debug("Sensor [%s] is talking Celsius", sensor['_id'])
        if last:
            return float(last['value'])
    elif unit in app.config['FAHRENHEIT_LABELS']:
        last = sensor.get('lastMeasurement', '')
        log.debug("Sensor [%s] is talking FAHRENHEIT. Converting.", sensor['_id'])
        if last:
            return fahrenheit_to_celsius(float(last['value']))
    return None


@retry(exceptions.RequestException, logger=log,
       tries=app.config['API']['retries'],
       max_delay=app.config['API']['max_delay'],
       jitter=app.config['API']['jitter'])
def get_single_box_data(boxid=app.config['BOXES'][0]):
    """
    Synchronously fetching data for single box
    :param boxid: id of the box in terms of openSenseMap
    :return: List of temperature readings from sensors of given box
    """
    log.debug("Got request for box_id: %s", boxid)
    api = app.config['API']
    request = get(api['prefix'] + "/" + boxid + "/" + api['suffix'], timeout=api['timeout'])
    request.raise_for_status()
    box_sensors = request.json()['sensors']
    log.debug("Got next sensors for box_id = %s: %s", boxid, box_sensors)
    return get_celsius(box_sensors)
