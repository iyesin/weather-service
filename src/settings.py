#!/usr/bin/env python3

"""
Settings container module
"""
from collections import namedtuple
from environs import Env

env = Env()
env.read_env()

env_defaults = {
    "api": {
        "prefix": "https://api.opensensemap.org/boxes",
        "suffix": "sensors",
        "timeout": 4,
        "retries": 5,
        "max_delay": 30,
        "jitter": 0.5
    },
    "boxes": [
        "5f5f5c133a337a001bfef1d3"
    ],
    "celsius_labels": ["°C", 'oC', '° C', "deg",
                       "deg C", "degree C", "degrees C", "°c",
                       "c", "'C", "*C", "° C",
                       "C", "C*", "C°", "Celcius",
                       "Celsius", "Unit: °C"
                       ],
    "fahrenheit_labels": ["°F", "F", "° F", ]
}

settings = {
    'API': env.dict('API', default=env_defaults['api']),
    'BOXES': env.list('BOXES', default=env_defaults['boxes']),
    'CELSIUS_LABELS': env.list('CELSIUS_LABELS', default=env_defaults['celsius_labels']),
    'FAHRENHEIT_LABELS': env.list('FAHRENHEIT_LABELS', default=env_defaults['fahrenheit_labels'])
}
NtSettings = namedtuple('nt_settings', 'API, BOXES, CELSIUS_LABELS, FAHRENHEIT_LABELS')
Settings = NtSettings(**settings)
