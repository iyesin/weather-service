#!/usr/bin/env python3

"""
WSGI wrapper module
It also configures logging for WSGI case
"""

from logging.config import fileConfig
from app import app

fileConfig("../logger.ini", disable_existing_loggers=True)

if __name__ == "__main__":
    app.run()
