FROM python:3.8-slim as builder

WORKDIR /ws
ENV PYTHONPATH=/ws/src DEBIAN_FRONTEND=noninteractive

ADD requirements.txt setup.py README.md logger.ini /ws/
ADD src /ws/src

RUN python3 setup.py install --force \
    && apt update && apt install --no-install-recommends -q -q -y ca-certificates busybox \
    && busybox --install /usr/local/bin \
    && rm -rf /tmp/* /var/cache

USER www-data
ENV BOXES=5cf9874107460b001b828c5b,5ca4d598cbf9ae001a53051a,59f8af62356823000fcc460c
ENV FLASK_APP=src/app.py FLASK_ENV=production
WORKDIR /ws/src

EXPOSE 8080
ENTRYPOINT waitress-serve wsgi:app
HEALTHCHECK --interval=3s --timeout=1s --start-period=3s CMD wget --quiet --output-document /dev/stdout http://127.1:8080/health
