# weather-service
Simple two end-point python service


## Objectives:
- expose `/health` endpoint with static reply (HTTP 200 OK)
- expose `/temperature` endpoint which will relay reply from openSenseMap service in JSON (see details below)


### Details about `/temperature` endpoint
This endpoint should respond with average temperature across all senseboxes specified in configuration in JSON format


## Ideas:
- pre-filter reply from opensensemap using icon-name (Work-around for localized field names)
- Use https://api.opensensemap.org/boxes/:senseBoxId/sensors as simplest entrypoint for querying data
- Cache data in memory for a less than with LRU to avoid throttling

## Usage
Default settings are set to match initial requirements.
Some defaults could be overridden with environment variables:
- BOXES - list of the boxes
- API - Dictionary of various API settings.
- CELSIUS_LABELS - list of "unit" field values that we treat as Celsius
- FAHRENHEIT_LABELS - list of "unit" field values that we treat as Fahrenheit

### Note on environment variables' format
All environment variables handled with [`environs` module](https://github.com/sloria/environs#basic-usage).

Please, consult with module documentation for details.

## Usage example
  ```
  docker run -p 8080:8080 BOXES=5cf9874107460b001b828c5b,5ca4d598cbf9ae001a53051a,59f8af62356823000fcc460c,5ee9c4bddc1438001b8aa942,5f63c5dd5189f4001b00f8a3 weather_service
  ```

