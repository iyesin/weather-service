# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from src.version import __version__

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(name='weather-service',
      version=__version__,
      description='Simple openSenseMap wrapper service',
      long_description=open('README.md').read(),
      classifiers=[
          'Development Status :: 1 - Proof of concept',
          'Intended Audience :: Developers',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
          'Programming Language :: Python :: 3',
      ],
      keywords='weather openSenseMap',
      author='Ilya Esin',
      author_email='yesin.iv@gmail.com',
      url='https://gitlab.co/iyesin/weather-service',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      # package_data={'': ['resources/*']},
      python_requires='>=3.5, <4',
      install_requires=required,
      include_package_data=True,
      # test_suite='tests',
      zip_safe=True,
      )
